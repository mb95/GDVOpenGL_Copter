/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Parts.cpp
 * Author: marco
 * 
 * Created on May 5, 2017, 3:35 PM
 */

#include "Parts.h"

Parts::Parts() {
}

Parts::Parts(const Parts& orig) {
}

Parts::~Parts() {
}

void Parts::makeBody() {
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    maker.Cylinder(0.5, 1.5, 1, 0.17, 0, 1);
    glPopMatrix();

    //--------------------------------------------------

    glPushMatrix();
    maker.Sphere(0.5, 0.5, 1, 1, 0.3);
    glPopMatrix();

    //--------------------------------------------------

    glPushMatrix();
    glTranslatef(1.5, 0, 0);
    maker.Sphere(0.5, 1, 0.17, 0, 1);
    glPopMatrix();

    //--------------------------------------------------
    
    glPushMatrix();
    glTranslatef(2.0, 0, 0);
    glRotatef(10, 0, 0, 1);
    glScalef(10, 1, 1);
    maker.Cube(0.3, 1, 0.17, 0, 1);
    glPopMatrix();
}

void Parts::makeBackRotor() {
    glPushMatrix();
    glScalef(0.1, 0.1, 0.1);
    glRotatef(90, 1, 0, 0);
    makeRotor();
    glPopMatrix();
}

void Parts::makeRotor() {
    glPushMatrix();
    glScalef(0.8, 0.8, 0.8);
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    maker.Cylinder(0.25, 0.3, 0, 0, 0, 1);
    glPopMatrix();

    //--------------------------------------------------

    glPushMatrix();
    glScalef(15, 0.4, 1);
    maker.Cube(0.5, 0, 0, 0, 1);
    glPopMatrix();

    //--------------------------------------------------

    glPushMatrix();
    glScalef(1, 0.4, 15);
    maker.Cube(0.5, 0, 0, 0, 1);
    glPopMatrix();
    glPopMatrix();
}

void Parts::makeShoes() {
    glPushMatrix();
    glRotatef(90, 1,0,0);
    glTranslatef(-1, 0, 0);
    maker.Cylinder(0.1,0.5, 0,0,0,1);
    glPopMatrix();
    
    glPushMatrix();
    glRotatef(90, 1,0,0);
    maker.Cylinder(0.1,0.5, 0,0,0,1);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(-1.5, -0.5, 0);
    glRotatef(90, 0,1,0);
    maker.Cylinder(0.1, 2, 0,0,0,1);
    glPopMatrix();
}
