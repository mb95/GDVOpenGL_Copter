/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Parts.h
 * Author: marco
 *
 * Created on May 5, 2017, 3:35 PM
 */

#ifndef PARTS_H
#define PARTS_H

#include "FigureMaker.h"

class Parts {
public:
    Parts();
    Parts(const Parts& orig);
    virtual ~Parts();
    void makeRotor();
    void makeBody();
    void makeBackRotor();
    void makeShoes();
private:
    FigureMaker maker; 

};

#endif /* PARTS_H */

