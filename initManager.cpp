/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   initManager.cpp
 * Author: marco
 * 
 * Created on May 11, 2017, 4:58 PM
 */

#include "initManager.h"

using std::cout;
using std::endl;
using std::string;

initManager::initManager() {
}

initManager::initManager(const initManager& orig) {
}

initManager::~initManager() {
}

GLuint initManager::loadTexture(std::string imgName) {
    // Textur einlesen usw.
    GLuint texture = SOIL_load_OGL_texture((const char*) imgName.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);

    if (texture == 0) {
        cout << imgName << " not found" << endl;
        exit(1);
    }
    else return texture;
}
