#include "Helicopter.h"

Helicopter::Helicopter() {
}

Helicopter::Helicopter(const Helicopter& orig) {
}

Helicopter::~Helicopter() {
}

void Helicopter::drawHelicopter(int rotorRotation) {

    glRotatef(-90, 0, 1, 0);
    glPushMatrix();
    p1.makeBody();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.5, 0.7, 0);
    glRotatef(rotorRotation, 0, 1, 0);
    p1.makeRotor();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(3.45, 0.3, 0.2);
    glRotatef(2 * rotorRotation, 0, 0, 1);
    p1.makeBackRotor();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(1.2, -0.4, 0.25);
    p1.makeShoes();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(1.2, -0.4, -0.25);
    p1.makeShoes();

    glPopMatrix();
}