/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   initManager.h
 * Author: marco
 *
 * Created on May 11, 2017, 4:58 PM
 */

#include <iosfwd>
#include <string>
#include <iostream>
#include <SOIL/SOIL.h>
#include <GL/freeglut.h>

#ifndef INITMANAGER_H
#define INITMANAGER_H

class initManager {
public:
    initManager();
    initManager(const initManager& orig);
    virtual ~initManager();
    GLuint loadTexture(std::string imgName);
private:

};

#endif /* INITMANAGER_H */

