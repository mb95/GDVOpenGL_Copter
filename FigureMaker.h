/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FigureMaker.h
 * Author: marco
 *
 * Created on May 5, 2017, 3:39 PM
 */

#ifndef FIGUREMAKER_H
#define FIGUREMAKER_H

#include <GL/freeglut.h>

class FigureMaker {
public:
    FigureMaker();
    FigureMaker(const FigureMaker& orig);
    virtual ~FigureMaker();
    void Cube(GLfloat sideLength, GLfloat r, GLfloat g, GLfloat b, GLfloat transparency);
    void Sphere(GLfloat radius, GLfloat r, GLfloat g, GLfloat b, GLfloat transparency);
    void Cylinder(GLfloat radius, GLfloat height, GLfloat r, GLfloat g, GLfloat b, GLfloat transparency);
private:

};

#endif /* FIGUREMAKER_H */

