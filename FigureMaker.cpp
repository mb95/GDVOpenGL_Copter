/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FigureMaker.cpp
 * Author: marco
 * 
 * Created on May 5, 2017, 3:39 PM
 */

#include "FigureMaker.h"

FigureMaker::FigureMaker() {
}

FigureMaker::FigureMaker(const FigureMaker& orig) {
}

FigureMaker::~FigureMaker() {
}

void FigureMaker::Cube(GLfloat sideLength, GLfloat r, GLfloat g, GLfloat b, GLfloat transparency) {
    glColor4f(r, g, b, transparency);
    glutSolidCube(sideLength);
}

void FigureMaker::Cylinder(GLfloat radius, GLfloat height, GLfloat r, GLfloat g, GLfloat b, GLfloat transparency) {
    glColor4f(r, g, b, transparency);
    glutSolidCylinder(radius, height, 50, 1);
}

void FigureMaker::Sphere(GLfloat radius, GLfloat r, GLfloat g, GLfloat b, GLfloat transparency) {
    glColor4f(r, g, b, transparency);
    glutSolidSphere(radius, 50, 50);
}
