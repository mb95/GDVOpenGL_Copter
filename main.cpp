#include <iostream>
#include "Helicopter.h"
#include "initManager.h"

using std::cout;
using std::endl;

int ih = 600;
int iw = 600;

float viewDepth = 30.0;

Helicopter h1;

float rotorRotation = 0;

//float heliXAngle, heliYAngle, heliZAngle, heliXRot, heliYRot, heliZRot = 0;
float helicopterYPosition = 0;
float helicopterXPosition = 0;
float helicopterZPosition = 0;

GLuint oceanTexture;
GLuint skyTexture;

void Init() {
    // Hier finden jene Aktionen statt, die zum Programmstart einmalig 
    // durchgeführt werden müssen

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHT0);
    glClearDepth(1.0);
    glClearColor(0, 0, 1, 1); // Hintergrundfarbe bit setzen

    initManager m;
    oceanTexture = m.loadTexture("ocean.png");
    //skyTexture = m.loadTexture("sky.png");
}

void orthogonalStart() {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(-iw / 2, iw / 2, -ih / 2, ih / 2);
    glMatrixMode(GL_MODELVIEW);
}

void orthogonalEnd() {
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

void renderBackground() {
    glBindTexture(GL_TEXTURE_2D, oceanTexture);
    glEnable(GL_TEXTURE_2D);
    orthogonalStart();
    glPushMatrix();
    glTranslatef(-iw / 2, -ih / 2, 0);
    glBegin(GL_QUADS);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex2i(0, 0);
    glTexCoord2f(1.0f, 0.0f);
    glVertex2i(iw, 0);
    glTexCoord2f(1.0f, 1.0f);
    glVertex2i(iw, ih);
    glTexCoord2f(0.0f, 1.0f);
    glVertex2i(0, ih);
    glEnd();
    glPopMatrix();
    orthogonalEnd();
    glDisable(GL_TEXTURE_2D);
}

void RenderScene() {
    //Zeichenfunktion
    //renderBackground();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity(); // Aktuelle Model-/View-Transformations-Matrix zuruecksetzen

    
    //renderBackground();
    
    glTranslatef(helicopterXPosition, helicopterYPosition, helicopterZPosition);
    h1.drawHelicopter(rotorRotation);
    gluLookAt(0, 2, 15, 0., 0., 0., 0., 1., 0.);

    glutSwapBuffers();
}

void Reshape(int width, int height) {
    // Hier finden die Reaktionen auf eine Veränderung der Größe des 
    // Graphikfensters statt


    // Matrix für Transformation: Frustum->viewport
    glMatrixMode(GL_PROJECTION);
    // Aktuelle Transformations-Matrix zuruecksetzen
    glLoadIdentity();
    // Viewport definieren
    glViewport(0, 0, width, height);
    // Frustum definieren (siehe unten)
    //glOrtho(-1, 1, -1, 1, 0, 1); 

    gluPerspective(45, 1., 0.1, viewDepth + 0.1);

    // Matrix für Modellierung/Viewing
    glMatrixMode(GL_MODELVIEW);

}

void Animate(int value) {
    // Hier werden Berechnungen durchgeführt, die zu einer Animation der Szene  
    // erforderlich sind. Dieser Prozess läuft im Hintergrund und wird alle 
    // 1000 msec aufgerufen. Der Parameter "value" wird einfach nur um eins 
    // inkrementiert und dem Callback wieder uebergeben. 


    rotorRotation += 10 % 360;


    glutPostRedisplay();
    // Timer wieder registrieren - Animate wird so nach 10 msec mit value+=1 aufgerufen.
    int wait_msec = 10;
    glutTimerFunc(wait_msec, Animate, ++value);
}

void keyboardHandler(unsigned char key, int x, int y) {
    switch (key) {
        case 'q':
            cout << "Ending copter" << endl;
            exit(0);
            break;
        case 'w':
            helicopterZPosition -= 0.04;
            break;
        case 'a':
            helicopterXPosition -= 0.04;
            break;
        case 's':
            helicopterZPosition += 0.04;
            break;
        case 'd':
            helicopterXPosition += 0.04;
            break;
        case 'u':
            helicopterYPosition += 0.04;
            break;
        case 'm':
            helicopterYPosition -= 0.04;
            break;
    }
}

int main(int argc, char **argv) {
    glutInit(&argc, argv); // GLUT initialisieren
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(ih, iw); // Fenster-Konfiguration
    glutCreateWindow("Lukas Piel; Marco Boncoraglio"); // Fenster-Erzeugung
    glutKeyboardFunc(keyboardHandler);
    glutDisplayFunc(RenderScene); // Zeichenfunktion bekannt machen
    glutReshapeFunc(Reshape);
    glutTimerFunc(10, Animate, 0);
    Init();
    glutMainLoop();
    glDeleteTextures(1, &oceanTexture);
    return 0;
}
