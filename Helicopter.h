#ifndef HELICOPTER_H
#define HELICOPTER_H

#include "Parts.h"

class Helicopter {
public:
    Helicopter();
    Helicopter(const Helicopter& orig);
    virtual ~Helicopter();
    void drawHelicopter(int rotorRotation);
private:
    Parts p1;
};

#endif /* HELICOPTER_H */

